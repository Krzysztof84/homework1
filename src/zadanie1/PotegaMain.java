package zadanie1;

public class PotegaMain {
    public static void main(String[] args) {
        System.out.println(potega(2, 13));
    }

    public static double potega(double wartosc, int potega) {
        if (potega == 0) {
            return 1;
        }
        return wartosc * potega(wartosc, potega - 1);
    }
}
