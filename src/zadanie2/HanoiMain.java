package zadanie2;

public class HanoiMain {

    public static void main(String[] args) {
        hanoi(3, 'A', 'B', 'C');
    }

    /**
     * Przeniesienie @{level} krążków z pachołka @{from} do pachołka @{to} używając jako pośredniego pachołka @{through}
     *
     * @param levels
     * @param from
     * @param through
     * @param to
     */
    public static void hanoi(int levels, char from, char through, char to) {
        if (levels == 1) {
            System.out.println("Przenoszę krążek z " + from + " do " + to);
        } else {
            hanoi(levels - 1, from, to, through);
            System.out.println("Przeniesienie " + levels + " z " + from + " do " + to);
            hanoi(levels - 1, through, from, to);
        }
    }
}
